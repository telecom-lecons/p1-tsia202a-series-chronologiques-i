# TSIA202a : Séries chronologiques (partie 1)

### Descriptif

Une série chronologique consiste en la modélisation d'une suite de valeurs
numériques par une suite de variables aléatoires statistiquement dépendantes. Ce
module introduit les concepts inhérents aux propriétés du second ordre:
autocovariance, densité et mesure spectrale, prédiction linéaire, processus des
innovations, ainsi que les modèles linéaires les plus couramment utilisés:
processus AR, MA, ARMA. Un deuxième volet du cours concerne les techniques
d'analyse spectrale qui trouvent des applications dans des domaines aussi variés
que l'analyse/synthèse sonore, l'analyse modale d'une vibration mécanique, la
sismologie, ou encore le traitement de signaux radar et sonar.

### Pré-requis

Probability

## Course presentation

This course introduces the theoretical foundations of random processes and time
series.

Each lesson (8.30 am - 10.00 am) is followed by tutorials (exercises/practical
works 10.15 am - 11.45 am).  Time series are used for modeling numerical
sequences. They consist in a sequence of statistically dependent random
variables. In this course, we focus on the concepts used for describing second
order properties : autocovariance function, spectral density measure or
function, linear prediction, innovation process, as well as the most popular
linear models: AR, MA, and ARMA processes. The second part of the course is
devoted to spectral analysis techniques. Various applications are targeted,
including speech synthesis, sismology or radar.
