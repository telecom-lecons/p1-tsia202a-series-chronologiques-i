# -*- coding: utf-8 -*-
"""
Created on Fri Sep 13 16:42:18 2019

@author: cagnazzo
"""

import numpy as np
from scipy import signal as sig
from scipy import linalg as la
import matplotlib.pyplot as plt
import randproc as rp
import time

#%%
plt.close('all')
n = 1000
p = 4

std = 1
X, phi = rp.genAR(p,n,std)
coeff= np.concatenate(([1], -phi))
rp.drawZ_DTFT_AR(X,phi)

#%%

mu = np.mean(X)
X_tilde = X - mu

epsilon = np.fft.fft(X_tilde)
periodogram = abs(epsilon)**2 / len(X) 

def In_specific(X, m):
    return abs(np.fft.fft(X_tilde, m))**2 / len(X)

def autocovariance(X, h):
    h = abs(h)
    if h >= len(X) or h==0:
        return 0
    X_t = X[:-h]
    X_th = X[h:]
    return (np.mean(X_t*X_th) - np.mean(X_t)*np.mean(X_th))

def autocov(X):
    result = np.zeros(len(X))
    for h in range(len(X)):
        result[h] = autocovariance(X,h)
    return result

def empirical_estimator(X, h):
    h = abs(h)
    if h >= len(X):
        return 0
    X_t = X[:-h]
    X_th = X[h:]
    return sum((X_t-mu)*(X_th-mu))/len(X)

#%%

gamma = autocov(X)
Gamma_hat = rp.acovb(X)

plt.plot(gamma)
plt.plot(Gamma_hat, 'r')
plt.show()

# v1 = np.zeros(p+1)
# v1[0]=1

  
# c = ???

 
# sigma2Est = ???
# estimated_coeff= ???
# estimated_coeff[0] = ???                  


# print estimated_coeff 
# print coeff            
# #%%
# err= coeff -     estimated_coeff
# rel_err =  np.linalg.norm(err)/np.linalg.norm(coeff) 
# print('Relative error {0:.2%} '.format(rel_err))


# #% Show spectra
# plt.figure()
# nPoints = np.int(np.exp2( np.ceil(np.log2(X.size))))
# nu = np.linspace(-0.5, 0.5 - 1/nPoints, nPoints)
# omega = 2*np.pi*nu
# w1, H1 = sig.freqz([1], coeff, omega)
# w2, H2 = sig.freqz([1], estimated_coeff, omega)
# plt.plot(w1,abs(H1))
# plt.plot(w2,abs(H2))
# plt.legend(['Amplitude Frequency Response (FR) of the AR filter','Estimated Amplitude FR' ])

# #%%Display poles
# plt.figure()
# # Draw the unit circle

# plt.axis('equal')
# plt.grid()
# zk    = np.roots(coeff)
# plt.plot(np.real(zk),np.imag(zk), 'x' )
# zk2 = np.roots(estimated_coeff)
# plt.plot(np.real(zk2),np.imag(zk2), '+' )
# plt.legend(['Poles of the AR transfer function','Poles of the estimated AR'])
# t = np.linspace(-np.pi,np.pi,1000)
# plt.plot(np.sin(t),np.cos(t), 'black')