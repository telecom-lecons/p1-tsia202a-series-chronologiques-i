# -*- coding: utf-8 -*-
"""
Created on Wed Nov 4 10:41:05 2020

@author: gbittencourt
"""
import numpy as np
from scipy import signal as sig
from scipy import linalg as la
import matplotlib.pyplot as plt
import randproc as rp
import time

#%% Initializations
H   = 40                    # number of points for covariance
n   = 2*H -1                # number of process' samples
tc  = np.arange(-(H-1),H)   # temporal axis for ACF
t   = np.arange(0,n)        # temporal axis for the signal
#%% Functions

def build_signal(signal,n,sigma=1,phi=0.6):
    H = int((n+1)/2)
    WN = np.random.normal(0,sigma**2,n) # White noise

    if signal=='AR':
        # Generate an AR(1) signal
        # Build the rational function P/Q
        Pcoeffs = np.array([1.])
        Qcoeffs = np.poly((phi,))
        # Use P/Q for filtering
        X = sig.lfilter(Pcoeffs, Qcoeffs, WN)
        
    elif signal=='SIN':
        #  Harmonic signal
        #A=2.;
        #A_0= -A+2*A*np.random.random_sample();
        A_0 = 1.
        omega = np.pi/3.
        phi =   np.pi +2*np.pi*np.random.random_sample();
        X = A_0 * np.cos(omega*tc+phi) + WN
    
    elif signal=='WN' : #  white noise
        X = WN

    # Compute empirical means
    EX     = np.zeros(n)          # Expectations are stored here
    EX = EX + X;

    # Compute exmpirical covariances
    gamma  = np.zeros(n)          # ACF's are stored here
    for h in range(H):
        # compute cov for lag = h
        XX  = X[0:n-H] * X[h:h+n-H]
        EXX =  XX.sum() / float(n-H)      # computation of the unbiased ACF estim
        gamma[h+H-1] = gamma[h+H-1] + EXX # gamma[H-1] = cov(X_n, X_n)
        gamma[H-h-1] = gamma[h+H-1].conj()     

    return X, EX, gamma


def plot_EX_ACF(EX, gamma, title):
    
    plt.figure()
    
    # Display sample averages
    plt.subplot(211)
    plt.title(title)
    line1, = plt.plot(t, EX, '-o' )
    plt.grid()
    plt.ylabel('Sample averages')
    
    #Display the unbiased sample covariance
    plt.subplot(212)
    line2, = plt.plot(tc, gamma, '-o' )
    plt.grid()
    plt.ylabel('Sample covariance')
    
    plt.show()


def periodogram(X, m):
    X_tilde = X - np.mean(X)
    epsilon = np.fft.fft(X_tilde, m)
    return abs(epsilon)**2 / len(X)


def autocov(X):
    result = np.ones(len(X))
    mu = np.mean(X)
    for h in range(1,len(X)):
        Xt = X[:-h]
        Xt_h = X[h:]
        result[h] = np.mean(Xt*Xt_h) - np.mean(Xt)*np.mean(Xt_h)
    return result


#%% Exercise 1

# 1.1 WN
WN, EX_wn, gamma_wn = build_signal('WN', n)
plot_EX_ACF(EX_wn, gamma_wn, 'WN')

# 1.2 AR(1)
AR, EX_ar, gamma_ar = build_signal('AR', n, phi=0.6)
plot_EX_ACF(EX_ar, gamma_ar, 'AR(1)')
rp.drawZ_DTFT_AR(AR,np.array([0.6]))

# 1.3 SIN
SIN, EX_sin, gamma_sin = build_signal('SIN', n)
plot_EX_ACF(EX_sin, gamma_sin, 'SIN')

#%% Exercise 2

# 2.2 Periodogram - In
m = 2*n
In_ar = periodogram(AR, m)
In_sin = periodogram(SIN, m)
In_wn = periodogram(WN, m)

plt.figure()

plt.subplot(311)
plt.title('Periodograms')
plt.plot(In_ar)
plt.ylabel('AR')
plt.xticks([], [])

plt.subplot(312)
plt.plot(In_sin)
plt.ylabel('SIN')
plt.xticks([], [])

plt.subplot(313)
plt.plot(In_wn)
plt.ylabel('WN')

plt.show()

# 2.3 ACF estimated
gamma = autocov(AR)
Gamma_hat = rp.acovb(AR)

plt.figure()
plt.title('ACF from AR(1)')
plt.plot(gamma)
plt.plot(Gamma_hat, 'r')
plt.legend(['Empirical ACF', 'Estimated ACF'])
plt.show()

# 2.4 WN periodogram variance

plt.figure()
plt.subplot(222)
plt.title('WN periodogram variances')

for n in range(1,5):
    N = 10**n
    WN_n, EX_wn_n, gamma_wn_n = build_signal('WN', N)
    PSD = np.mean((WN_n - np.mean(WN_n))**2)
    omegas = np.linspace(0, 2*np.pi, N)
    Sxx = np.abs(np.fft.fft(WN_n))**2
    var_Ixx = Sxx**2 * np.exp(1j*omegas) * (1 + (np.sin(N*omegas) / (N*np.sin(omegas)))**2 )
    plt.subplot(2,2,n)
    plt.ylim([0, 1e7])
    if (n%2 == 0):
        plt.yticks([])
    plt.plot(abs(var_Ixx))
    plt.ylabel('N = ' + str(N))

plt.show()

#%% Exercise 3 - Part II

## 3.II.1

n = 1000
p = 4
std = 1
X, phi = rp.genAR(p, n, std)
coeff = np.concatenate(([1], -phi))
rp.drawZ_DTFT_AR(X,phi)

## 3.II.2

gamma_hat = rp.acovb(X)
Gamma_matrix = la.toeplitz(gamma_hat)[:p+1,:p+1]
plt.figure()
plt.imshow(Gamma_matrix)
plt.title('Estimated Gamma matrix of AR(' + str(p) +')')
plt.show()

## 3.II.3

v1 = np.zeros(p+1)
v1[0] = 1
sigma2Est = gamma_hat[0]
estimated_coeff = np.matmul(la.inv(Gamma_matrix), v1)
estimated_coeff[0] = 1
err = coeff - estimated_coeff
rel_err =  np.linalg.norm(err)/np.linalg.norm(coeff)

print (estimated_coeff)
print (coeff)
print('Relative error {0:.2%} '.format(rel_err))

plt.figure()
plt.subplot(211)
plt.title('Original AR filter vs Estimated AR model (err={0:.2%})'.format(rel_err))
nPoints = np.int(np.exp2( np.ceil(np.log2(X.size))))
nu = np.linspace(-0.5, 0.5 - 1/nPoints, nPoints)
omega = 2*np.pi*nu
w1, H1 = sig.freqz([1], coeff, omega)
w2, H2 = sig.freqz([1], estimated_coeff, omega)
plt.plot(w1,abs(H1))
plt.plot(w2,abs(H2))
plt.ylabel('PSD')
plt.legend(['AR filter','Estimated AR '])

plt.subplot(212)
plt.axis('equal')
plt.grid()
zk    = np.roots(coeff)
plt.plot(np.real(zk),np.imag(zk), 'x' )
zk2 = np.roots(estimated_coeff)
plt.plot(np.real(zk2),np.imag(zk2), '+' )
plt.ylabel('Poles')
plt.legend(['AR filter','Estimated AR'])
t = np.linspace(-np.pi,np.pi,1000)
plt.plot(np.sin(t),np.cos(t), 'black')
