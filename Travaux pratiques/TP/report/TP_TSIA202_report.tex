\documentclass{article}
\usepackage[utf8]{inputenc}

\usepackage{amsmath}
\usepackage{amssymb}

\usepackage{float}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}

\usepackage{geometry}
\geometry{a4paper,left=20mm,top=20mm,right=20mm,bottom=20mm,}

\usepackage{listings}
\usepackage{xcolor}

\definecolor{codegreen}{rgb}{0,0.6,0}
\definecolor{codegray}{rgb}{0.5,0.5,0.5}
\definecolor{codepurple}{rgb}{0.58,0,0.82}
\definecolor{backcolour}{rgb}{0.95,0.95,0.92}

\lstdefinestyle{mystyle}{
    backgroundcolor=\color{backcolour},
    commentstyle=\color{codegreen},
    keywordstyle=\color{magenta},
    numberstyle=\tiny\color{codegray},
    stringstyle=\color{codepurple},
    basicstyle=\ttfamily\footnotesize,
    breakatwhitespace=false,
    breaklines=true,
    captionpos=b,
    keepspaces=true,
    numbers=left,
    numbersep=5pt,
    showspaces=false,
    showstringspaces=false,
    showtabs=false,
    tabsize=2
}

\lstset{style=mystyle}


\title{Time Series TSIA202a - Practical work}
\author{Gabriela Bittencourt}
\date{Nov 2020}

\begin{document}

\maketitle

Read the report while execute the codes produced for a better comprehension.

\section{COMPUTING AVERAGES AND COVARIANCES}

\subsection*{\normalsize 1.1. White noise of variance $\sigma^2 = 1$ and $N=80$}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.5\textwidth]{ex1_WN.png}
	\caption{Sample average and covariance of a white noise zero-mean and variance
	$\sigma^2 = 1$}
	\label{fig:ex1wn}
\end{figure}

For a white noise process we have $\gamma_x(h) = \sigma^2_x\delta(h)$. Therefore
we expect to have a graph of covariance samples with a pick of $\sigma_x^2 = 1$
in $h = 0$ and zeros in all other values of $h$. In Figure~\ref{fig:ex1wn} we
observe that the value of $h = 0$ is correct, but we see that are some
covariance measured in the other values of $h$ that should be zero. For solving
that, all the covariances must be greater than a threshold to be considered
non-zero.

\subsection*{\normalsize 1.2. Causal AR($1$) process with a coefficient $\phi \in
]-1,1[$ and $N=80$}

We have a AR($1$) process:

\begin{equation*}
	X_t = \phi X_{t-1} + Z_t
\end{equation*}
where $Z_t$ is a white noise process with variance $\sigma_z^2$.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.5\textwidth]{ex1_AR.png}
	\caption{Sample average and covariance of a causal AR($1$) process}
	\label{fig:ex1ar}
\end{figure}

For a causal zero-mean auto-regressive process AR($1$), with coefficient $\phi$,
we have $\gamma_x(h) = \frac{\sigma_z^2}{1-\phi^2}\phi^{|h|}$. Knowing that
$\phi \in ]-1,1[$, $\gamma_x$ is maximum when $h = 0$, $\gamma_{x_{max}} =
\frac{\sigma_z^2}{1-\phi^2}$. And we expected to see it decreasing over time
lag.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.5\textwidth]{ex1_AR_dtft.png}
	\caption{Poles and power spectral density of a causal AR($1$) process}
	\label{fig:ex1ar_dtft}
\end{figure}

We observe that the pole is inside of the unit circle, this is due to the fact
that our AR($1$) filter is a causal process.

\subsection*{\normalsize 1.3. Sinusoidal process $X_t = A_0\cos(\lambda_0t+\Phi_0) +
	Z_t$, with $\lambda \in [0,\pi[$ and $\Phi_0$ a uniform random
			variable in $[0,2\pi]$, independent of $Z_t$ and $N=80$}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.5\textwidth]{ex1_SIN.png}
	\caption{Sample average and covariance of a sinusoidal process}
	\label{fig:ex1sin}
\end{figure}

For a sinusoidal process, we have $\gamma_x(h) =
\frac{A_0^2}{2}\cos(\lambda_0h) + \sigma_z^2\delta(h)$. Therefore, we expect a
cosinusoidal shape in the covariance graph, and that's what we observe in
Figure~\ref{fig:ex1sin}.

\section{SPECTRAL DENSITY ESTIMATION AND PERIODOGRAM}

\subsection*{\normalsize 2.1. Empirical estimator $\hat{\gamma}_n(h)$}

As proved in class, we have that the $\mathsf{DTFT}$ of centred samples is:

\begin{equation*}
	\xi(\lambda) = \frac{1}{2\pi}\sum^{n-1}_{t=0}\tilde{X}_te^{-i\lambda t}
\end{equation*}
where $\tilde{X}_t = X_t - \hat{\mu}_n$ and $\hat{\mu}_n$ is the expected value
of $X_t$.

So we can express the periodogram as:

\begin{equation}
	I_n(\lambda) = \frac{2\pi}{n}\ |\xi(\lambda)|^2
	\label{eqn:periodogram}
\end{equation}

\subsection*{\normalsize 2.2. $I_n(2\pi\frac{k}{m})$}

Applying the function of the periodogram expressed in
Eq~\eqref{eqn:periodogram}, substituting $\lambda$ for $2\pi\frac{k}{m}$ is
equivalent to make a $\mathsf{DTFT}$ of $I_n$ with zero-padding with $m$
elements. Applying it in the signals created in exercise 1, we have the results
presented in Figure~\ref{fig:periodogram_signal_ex1}.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.6\textwidth]{ex2_periodogram_signals.png}
	\caption{Periodogram from signals created in exercise 1}
	\label{fig:periodogram_signal_ex1}
\end{figure}

\subsection*{\normalsize 2.3. Obtaining $\hat{\gamma}_n(h)$ from
$I_n(2\pi\frac{k}{m})$}

The autocovariance can be estimated ($\hat{\gamma}_x(h)$) through the inverse
Fourrier transform of $I_n$. The autocovariance estimated is compared with the
empirical autocovariance in Figure~\ref{fig:gamma_hat}.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.6\textwidth]{ex2_ACF.png}
	\caption{Comparison of the empirical ACF $\gamma$ in blue and the estimated
	ACF $\hat{\gamma}$ in red of a AR(1) process.}
	\label{fig:gamma_hat}
\end{figure}

We observe that as we increase the value of $h$ the autocovariance estimated
approches the zero, that is the mathematically expected value, as showed in
exercise 1.2. while the empirical autocovariance oscillates. Therefore, we
conclude that the autocovariance estimated is more reliable.

\subsection*{\normalsize 2.4. Variance of the periodogram}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\textwidth]{ex2_periodogram.png}
	\caption{Periodogram variances of a white noise for different number of
	samples (N)}
	\label{fig:periodogram_multi_n}
\end{figure}

For the same amplitude of variance scale, thus vertical scale of the graphs
(from 0 to 1000000), we can observe that as we increase the number of samples
(N), we increase the amplitude of the periodogram variance.

We can analyse mathematically the expression for the variance of a periodogram
of a Gaussian process:

\begin{equation*}
	var\{I_{xx}\} = S^2_{xx}e^{j\Omega}\cdot \left[ 1 +
	\left(\frac{\sin(N\Omega)}{N\sin(\Omega)}\right)^2 \right]
\end{equation*}

As $N \rightarrow \infty$ we have the expression $\lim_{(N \rightarrow \infty)}
\frac{\sin(N\Omega)}{N\sin(\Omega)} = 0$. Therefore we can deduce that as we
increase the number of samples, the expression of the variance of the
periodogram approches $S^2_{xx}e^{j\Omega}$, where $S_{xx}$ is the power
spectral density (PSD). So we would expect that the variance of the periodogram
increases with the increase of $N$, as we observe in
Figure~\ref{fig:periodogram_multi_n}.

\section{FILTERING RANDOM PROCESSES}

\subsection*{Part I -- Yule-Walker equation}

Auto-regressive model: causal, zero-mean AR($p$) process.

\begin{equation}
	X_t = \phi_1X_{t-1} + \phi_2X_{t-2} + \cdots + \phi_pX_{t-p} + Z_t
	\label{eqn:ar}
\end{equation}

Process $Z_t$ is a white-noise of variance $\sigma^2$.

\subsubsection*{3.I.1. Showing that $\forall h \geq 1,\
\mathbb{E}\left[X_{t-h}Z_t\right] = 0$}

We know that for a causal AR($p$) process we have: $X_t =
\sum^p_{k=1}\phi_kX_{t-k} + Z_t$. Therefore, $X_t$ depends on the past events of
the process $Z$, however it doesn't depend on future measures of $Z$. In other
words,$\forall k \in \{0,1,...,t\}$, $X_t$ depends on $Z_{t-k}$, but it is
independent of all $Z_{t+k}$.

So we can say that $X_{t-h}$ and $Z_t$ are independent processes, thus
$\mathbb{E}\left[X_{t-h}Z_t\right] =
\mathbb{E}\left[X_{t-h}\right]\mathbb{E}\left[Z_t\right]$. As $X_t$ is a
zero-mean process, $\mathbb{E}[X_{t-h}] = 0$, so:

\begin{equation*}
	\mathbb{E}\left[X_{t-h}Z_t\right] =
	\mathbb{E}\left[X_{t-h}\right]\mathbb{E}\left[Z_t\right] = 0
\end{equation*}

\subsubsection*{3.I.2. and 3.I.3. Recurent relation between $\gamma(h)$ and
$\gamma(h-1), \cdots, \gamma(h-p)$}

We can deduce the auto-correlation of Eq~\eqref{eqn:ar} as:

\begin{align*}
	\gamma_x(h) &= \mathsf{Cov}(X_{t+h}, X_t) \\
	&= \mathsf{Cov}(\phi_1X_{t+h-1} + \phi_2X_{t+h-2} + \cdots + \phi_pX_{t+h-p} +
	Z_t, X_t) \\
	&= \phi_1\mathsf{Cov}(X_{t+h-1},X_t) + \cdots +
	\phi_p\mathsf{Cov}(X_{t+h-p},X_t) + \mathsf{Cov}(Z_{t+h},X_t)
\end{align*}
where we know that:
\begin{align*}
	\mathsf{Cov}(Z_{t+h},X_t) &= \mathbb{E}\left[Z_{t+h}X_t\right] -
	\mathbb{E}\left[Z_{t+h}\right]\mathbb{E}\left[X_t\right] =
	\mathbb{E}\left[Z_{t+h}X_t\right] \\
														&=
														\begin{cases}
															\sigma^2 \qquad &\textrm{, for h = 0} \\
															0 \qquad &\textrm{ otherwise }
														\end{cases}
\end{align*}

We also know that $\mathsf{Cov}(X_{t+h-k},X_t) = \gamma_x(h-k), \forall k \in
\{1,2,\cdots,p\}$. So, we can write the $\gamma_x(h)$ as:

\begin{equation*}
	\gamma_x(h) = \sum^p_{k=1}\phi_k\gamma_x(h-k) + \delta(h)\sigma^2
\end{equation*}

or, equivalently:

\begin{equation*}
	\gamma_x(h) - \sum^p_{k=1}\phi_k\gamma_x(h-k) = \delta(h)\sigma^2
\end{equation*}

\subsubsection*{3.I.4. Matricial form of the $\gamma$s relations}

\begin{equation}
	\begin{bmatrix}
		\gamma_x(0) & \gamma_x(-1) & \cdots & \gamma_x(-p)\\
		\gamma_x(1) & \gamma_x(0) & \cdots & \gamma_x(1-p)\\
		\vdots & \vdots & \ddots & \vdots \\
		\gamma_x(p) & \gamma_x(p-1) & \cdots & \gamma_x(0)
	\end{bmatrix}
	\begin{bmatrix}
		1 \\
		-\phi_1 \\
		\vdots \\
		-\phi_p
	\end{bmatrix}
	=
	\begin{bmatrix}
		\sigma^2 \\
		0 \\
		\vdots \\
		0
	\end{bmatrix}
	\label{eqn:gamma_matrix}
\end{equation}

\subsection*{Part II -- Estimation}

\subsubsection*{3.II.1. Generate $n=1000$ samples of an AR($4$)}

Generating a random AR($4$) process of $n = 1000$, we have its transfer function
represented in Figure~\ref{fig:psd_ar4_1}.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.6\textwidth]{ex3_ii_psd1.png}
	\caption{Poles and PSD of a created AR($4$) process' transfer function}
	\label{fig:psd_ar4_1}
\end{figure}

\subsubsection*{3.II.2. Gamma estimated matrix}

We can create the autocorrelation matrix proposed in
Eq~\eqref{eqn:gamma_matrix}. In Figure~\ref{fig:gamma_hat_ar4_1} we can observe
that it's a Toeplitz matrix.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.3\textwidth]{ex3_ii_gamma1.png}
	\caption{Gamma matrix}
	\label{fig:gamma_hat_ar4_1}
\end{figure}

\subsubsection*{3.II.3. Comparison between the original AR($4$) created and the
Estimated AR($4$) modeling}

Now, we can compare the estimated model of a AR($4$) process with the created
signal, as presented in Figure~\ref{fig:poles_ar4_1}. We see that for a relative
error of $4.53\%$, both power spectral density curves are very similar and the
poles are in almost the same place.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\textwidth]{ex3_ii_poles1.png}
	\caption{Comparison of the AR($4$) signal created and the estimated model}
	\label{fig:poles_ar4_1}
\end{figure}

Depending on the random AR generated, we observe a model with bigger errors, as
presented in Figure~\ref{fig:ex3_exp2}.

\begin{figure}[H]
	\centering
	\begin{subfigure}[b]{0.48\textwidth}
		\centering
		\includegraphics[width=\textwidth]{ex3_ii_psd2.png}
		\caption{Transfer function of another model AR($4$)}
		\label{fig:gamma_hat_ar4_2}
	\end{subfigure}
	\hfill
	\begin{subfigure}[b]{0.48\textwidth}
		\centering
		\includegraphics[width=\textwidth]{ex3_ii_poles2.png}
		\caption{Comparison between original and estimated}
		\label{fig:poles_ar4_2}
	\end{subfigure}
	\caption{Estimated process with a relative error of $26.15\%$}
	\label{fig:ex3_exp2}
\end{figure}

\subsection*{Part III -- Application to speech signal}

We divided a signal audio into frames of $N_x$ samples and modelled each frame
as an AR($12$) process, using the concepts discussed in the previous sections.
In Figure~\ref{fig:ex3_analyse} we see some exemples of the modelling for 4
different frames.

\begin{figure}[H]
	\centering
	\begin{subfigure}[b]{0.4\textwidth}
		\centering
		\includegraphics[width=\textwidth]{ex3_iii_frame23.png}
	\end{subfigure}
	\begin{subfigure}[b]{0.4\textwidth}
		\centering
		\includegraphics[width=\textwidth]{ex3_iii_frame36.png}
	\end{subfigure}
	\begin{subfigure}[b]{0.4\textwidth}
		\centering
		\includegraphics[width=\textwidth]{ex3_iii_frame65.png}
	\end{subfigure}
	\begin{subfigure}[b]{0.4\textwidth}
		\centering
		\includegraphics[width=\textwidth]{ex3_iii_frame72.png}
	\end{subfigure}
	\caption{Exemples of AR($12$) for some frames of the audio}
	\label{fig:ex3_analyse}
\end{figure}

In Figure~\ref{fig:result} we can observe the application of all the concepts
developed in this practical work applied in a real signal of audio. In this step
we were able to analyse an audio and compress it $320/26 = 12.3$ times, thus
achieving a data rate saving of $0.92$ (compressed each frame of audio into 12
coefficients + the pitch information). The next step we synthesize it, producing
an audio signal using those 12 coefficients of the AR model and the pitch
information.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.75\textwidth]{ex3_iii_result.png}
	\caption{Comparison of the audio signal and the synthesized audio}
	\label{fig:result}
\end{figure}

To synthesize the audio it was first created a pulse train with its frequency
corresponding to the pitch information in each of the frames that has voiced
audio. Secondly, it was created an AR($12$) filter with the coefficients for
each especific frame and it was applied to the pulse train and to a white noise.
After that, the signal produced for each frame was modulated by a Hann window,
to avoid audio artifacts when summing overlapped frames.

\pagebreak
\appendix

There was some alterations in the given codes.

\pagenumbering{roman}
\section{Code for estimation and covariance codes} \label{sec:code_estimation}

The code for estimation and covariance were put together in the same file.

\lstinputlisting[language=python]{../est_cov.py}

\section{Code for speech} \label{sec:code_speech}

\lstinputlisting[language=python]{../speech_proc.py}

\end{document}
