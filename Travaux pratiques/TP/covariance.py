# -*- coding: utf-8 -*-
"""
Created on Fri Sep 13 10:32:40 2019

@author: cagnazzo
"""

import numpy as np
from scipy import signal as sig
import matplotlib.pyplot as plt
import randproc as rp
import estimation as est

#%% Initializations
H   = 20      # number of points for covariance
n   = 2*H -1  # number of process' samples

EX     = np.zeros(n)          # Expectations are stored here
gamma  = np.zeros(n)          # ACF's are stored here
tc     = np.arange(-(H-1),H)  # temporal axis for ACF
t      = np.arange(0,n)       # temporal axis for the signal

# Choose different signals here
signal = 'AR' # 'AR' 'WN' 'SIN'
sigma = 1

#%% Build Signal
WN = np.random.normal(0,sigma**2,n) # White noise

if signal=='AR':
    # Generate an AR(1) signal
    # Build the rational function P/Q
    phi1  = 0.6
    Pcoeffs = np.array([1.])
    Qcoeffs = np.poly((phi1,))
    # Use P/Q for filtering
    X = sig.lfilter(Pcoeffs, Qcoeffs, WN)
    rp.drawZ_DTFT_AR(X,np.array([phi1]))
    
elif signal=='SIN':
    #  Harmonic signal
    #A=2.;
    #A_0= -A+2*A*np.random.random_sample();
    A_0 = 1.
    omega = np.pi/3.
    phi =   np.pi +2*np.pi*np.random.random_sample();
    X = A_0 * np.cos(omega*tc+phi) + WN

elif signal=='WN' : #  white noise
    X = WN

# Compute empirical means
EX = EX + X;

# Compute exmpirical covariances
for h in range(H):
    # compute cov for lag = h
    XX  = X[0:n-H] * X[h:h+n-H]
    EXX =  XX.sum() / float(n-H)      # computation of the unbiased ACF estim
    gamma[h+H-1] = gamma[h+H-1] + EXX # gamma[H-1] = cov(X_n, X_n)
    gamma[H-h-1] = gamma[h+H-1].conj()     

#%%
m=2*n
In_spc = est.In_specific(X, m)

plt.figure(2,[H, 8])

# Draw the filter energy transfer function |H(nu)|^2
coeff = np.concatenate(([1], -np.array([phi1])))
nu = np.linspace(-0.5, 0.5 - 1/m, m)
omega = 2*np.pi*nu

w, H2 = sig.freqz([1], coeff, omega)
H3 = np.real( H2*np.conj(H2))
plt.plot(omega,H3/max(H3))
plt.plot(omega, In_spc)

plt.show()

#%% Plot results

# Create the figure where the results will be shown        
plt.figure(3,[H, 8])

# Display sample averages
plt.subplot(211, label='mean')
line1, = plt.plot(t, EX, '-o' )
plt.grid()
plt.title('Sample averages')

#Display the unbiased sample covariance
plt.subplot(212, label='ACF')
line2, = plt.plot(tc, gamma, '-o' )
plt.grid()
plt.title('Sample covariance')
plt.xticks(tc)

plt.show()