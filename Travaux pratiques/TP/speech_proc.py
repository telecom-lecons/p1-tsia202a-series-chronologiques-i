# -*- coding: utf-8 -*-
"""
Created on Fri Sep 13 16:42:18 2019

Implementation of a analysis/synthesis system for speech signal

@author: cagnazzo
"""

import numpy as np
from scipy import signal as sig
import matplotlib.pyplot as plt
import matplotlib.widgets as wid
import randproc as rp
import wave
from scipy import linalg as la

#%% Read sound data from file
filename = 'phrasezoe8k.wav'

wavObject = wave.open(filename)
nchannels, sampwidth, framerate, nframes, comptype, compname    = wavObject.getparams()

s = wavObject.readframes(nframes)
samples =  np.frombuffer(s, dtype='int16')/32768.
 
# Pre-emphasis
x = sig.lfilter([1, -0.98], [1], samples)

plt.figure()
plt.plot(samples)
plt.plot(x)

#%%
# Analysis parameters

frameLen = 0.04 # frame lenght in seconds
p        =   12 # AR parameter model
overlap  =   50 # frame overlap in %
Fmax     =  200 # maximum voicing frequency
Fmin     =   60 # minimum voicing frequency

Nx       = int(frameLen*framerate) # lenght of the frames in number of samples
lag      = int(Nx * overlap / 100)
framesToProc  = int((nframes-Nx)/lag -0.5) # floor
minT     = int(framerate/Fmax -0.5)  # floor
maxT     = int(framerate/Fmin + 0.5) # ceil
                   
synth    = np.zeros_like(samples)
pitch    = np.zeros(framesToProc)
coeff    = np.zeros([p,framesToProc])
sigma2   = np.zeros(framesToProc)

#%% Loop over frames
synth    = np.zeros_like(samples)
position = 0;
for frameIndex in  np.arange(framesToProc):
    startSample = frameIndex*lag
    stopSample  = startSample + Nx
    frame = x[startSample:stopSample]
    
    ### ANALYSIS
    ## Pitch detection
    pitch[frameIndex] = rp.detectPitch(frame, minT, maxT)
    
    ### AR estimation   
    gamma_hat = rp.acovb(frame)
    Gamma_matrix = la.toeplitz(gamma_hat)[:p+1,:p+1]   
    v1 = np.zeros(p+1)
    v1[0] = 1
    sigma2Est = gamma_hat[0]
    coeff_est = np.matmul(la.inv(Gamma_matrix), v1)
    coeff[:,frameIndex] = np.matmul(la.inv(Gamma_matrix), v1)[1:]
    sigma2[frameIndex] = gamma_hat[0]
   
    # Graphical check
    if True:
        nPoints = 2**9
        FRAME = np.fft.fft(frame, 2*nPoints)
        XF    = 10*np.log10((abs(FRAME[0:nPoints])**2)/Nx)
        nu = np.linspace(0, 0.5 - 1/nPoints, nPoints)
        omega = 2*np.pi*nu
        wAR, XAR = sig.freqz([1], coeff_est, omega) 
        XAR   = 20*np.log10(abs(XAR[0:nPoints])/np.sqrt(sigma2Est))
        
        plt.figure()
        plt.ylim([-80, 0])
        plt.plot(nu,XF)
        plt.plot(nu,XAR)
        plt.legend(['Empyirical PSD', 'PSD from AR model'])
        plt.title('Analysis of frame %4d'%frameIndex)
        plt.show()
        plt.pause(0.0001)        
        
    #coeff_est /= coeff_est[0]
    ## SYNTHESIS
    if pitch[frameIndex]: # Voiced sound
        T = pitch[frameIndex] - (pitch[frameIndex]%2)
        pT = np.zeros(Nx+p)
        pT[position::int(T)]=0.707  # "Dirac comb"or rather pulse train
        pT[position+int(T/2)::int(T)]=-0.707  # "Dirac comb"or rather pulse train          
        position = int( (Nx+p-position) % T)
        xs = sig.lfilter([1], coeff_est, pT)   
    else: # non voiced sound
        Z = np.random.normal(0, 1, Nx+p) # White noise          
        xs = sig.lfilter([1], coeff_est, Z)
        position = 0
        
    xs = xs[p::]
    normFact = np.sqrt(sigma2[frameIndex]/np.var(xs))
    xs = np.array(normFact * xs)
    synth[startSample:stopSample] += xs * rp.myhann(Nx)
    
#De-emphasis filter
synth = sig.lfilter([1], [1, -0.98], synth)

#%% Write on WAV file
data = synth*(2**15)
wavObject2 = wave.open('synth.wav','wb')

wavObject2.setparams((nchannels, sampwidth, framerate, nframes, "NONE", "Uncompressed"))
wavObject2.writeframes(data.astype('int16').tostring())
wavObject2.close()

#%%
plt.figure()
plt.subplot(311)
t = np.arange(nframes,dtype=float)/framerate
plt.plot(t,samples)
plt.ylabel('original audio')

tp = np.arange(framesToProc,dtype=float)*lag/framerate
plt.subplot(312)
plt.plot(tp,pitch)
plt.ylabel('pitch')
plt.subplot(313)
plt.plot(t,synth)
plt.ylabel('synthesized')